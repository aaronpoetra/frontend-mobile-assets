/* Sticky PDP Submenu */
// (function() {
//   var reset_scroll;
//   $(function() {
//     return $(".sub-menu-sticky").stick_in_parent({
//       parent: "#product-parent-stick",
//       spacer: false
//     });
//   });
// }).call(this);
/* End Sticky PDP Submenu */
$(window).load(function() {
  /* Sticky Top */
  // $("#top-sticky").sticky({
  //   topSpacing: 0,
  //   className: "top-main-navigation-slided"
  // });
  // $("#top-sticky-transaction").sticky({
  //   topSpacing: 0,
  //   className: "top-main-transaction-slided"
  // });
  // /* Sticky PDP Subnavigation */
  // $("#sticky-subnavigation").sticky({
  //   topSpacing: 70
  // });
  /* Main Slider */
  $('.slider-images').owlCarousel({
    loop: false,
    nav: false,
    items: 1,
    dots: true,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: false,
    rewind: true
  });
  /* banner side */
  $('.bannerSlider').owlCarousel({
    loop: false,
    nav: true,
    navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>'],
    autoplay: true,
    items: 1,
    dots: true
  });
  /* review side */
  $('.reviewSlider').owlCarousel({
    loop: true,
    nav: false,
    navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>'],
    autoplay: true,
    items: 1,
    dots: true
  });
  /* Owl Brand*/
  $('.owl-brand').owlCarousel({
    loop: false,
    nav: true,
    items: 1,
    dots: false,
    navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>']
  });
  /* Owl Main Slider 2*/
  $('.mainBanner-2').owlCarousel({
    loop: false,
    nav: true,
    items: 1,
    dots: true,
    navText: ['<i class="anticon icon-left"></i>', '<i class="anticon icon-right"></i>']
  });
});
$(document).ready(function() {
  /* Popover */
  $('[data-toggle="popover"]').popover({
    placement: 'top'
  });
  /* Search Click */
  // $('.js-show-panel').click(function() {
  //   $('.search-clicked').fadeIn('medium');
  //   $('.overlay-dark-search').toggle();
  //   $('.search-bar .search-btn').hide();
  //   $('.search-bar .close-btn').show();
  //   $(document).bind('focusin.search-clicked, click.search-clicked', function(e) {
  //     if ($(e.target).closest('.search-clicked, .js-show-panel').length) return;
  //     $(document).unbind('.search-clicked');
  //     $('.search-clicked').fadeOut('medium');
  //     $('.search-bar .search-btn').show();
  //     $('.search-bar .close-btn').hide();
  //     $(".search-bar").find('.overlay-dark-search').hide();
  //   });
  //   $('.js-show-panel').on('input', function() {
  //     if ($(this).val().length === 0) {
  //       $('.search-clicked').fadeIn('medium');
  //       $('.search-typed').fadeOut('medium');
  //       $('.search-bar .search-btn').hide();
  //       $('.search-bar .close-btn').show();
  //     } else {
  //       $('.search-clicked').hide();
  //       $('.search-typed').fadeIn('medium');
  //       $('.search-bar .search-btn').hide();
  //       $('.search-bar .close-btn').show();
  //       $("#val-search").text($(this).val());
  //       $(document).bind('focusin.search-typed, click.search-typed', function(e) {
  //         if ($(e.target).closest('.search-typed, .js-show-panel').length) return;
  //         $(document).unbind('.search-typed');
  //         $('.search-typed').fadeOut('medium');
  //         $('.search-bar .search-btn').hide();
  //         $('.search-bar .close-btn').show();
  //         $(".search-bar").find('.overlay-dark-search').hide();
  //       });
  //     }
  //   });
  // });
  $(".search-bar .close-btn").click(function() {
    $(".search-clicked").hide();
    $(".search-typed").hide();
    $(".search-bar").find("input").val("");
    $(".search-bar .close-btn").hide();
  });
  /* Product Variant */
  $(".product-container .color-container").click(function() {
    var attrcolor = $(this).attr("attr-color");
    $(this).closest(".product-container .bitmap").find(".color-container").removeClass("selected");
    $(this).addClass("selected");
    $(this).closest(".product-container .bitmap").find(".item").removeClass("selected");
    $(this).closest(".product-container .bitmap").find("#" + attrcolor).addClass("selected");
  });
  /* Color Filter */
  $("#color-filter-choose>li").click(function() {
	$("#color-filter-choose>li").removeClass("active");
    var datacolor = $(this).attr("data-color");
	$(this).addClass("active");
    $("#color-filter").val(datacolor);
  });
  /* Fancybox */
  $('.fancybox').fancybox();

  /* Color Variant product */
  $("#cat_color li").click(function() {
    $("#cat_color li").removeClass("active");
    var dataval = $(this).find("a").attr("data-attr");
    $("#" + dataval).addClass("active");
    $("#color_choose").val(dataval);
  });
});
/* Submenu Navigation scroll and click */
$(document).ready(function() {
  $(document).on("scroll", onScroll);
  $('.sub-navigation-sticky a').on('click', function(e) {
    e.preventDefault();
    $(document).off("scroll");
    $('a').each(function() {
      $(this).removeClass('active');
    })
    $(this).addClass('active');
    var target = this.hash;
    $target = $(target);
    $('html, body').stop().animate({
      'scrollTop': $target.offset().top + 100
    }, 600, 'swing', function() {
      window.location.hash = target;
      $(document).on("scroll", onScroll);
    });
  });
});

function onScroll(event) {
  var scrollPosition = $(document).scrollTop();
  $('.sub-navigation-sticky a').each(function() {
    var currentLink = $(this);
    var refElement = $(currentLink.attr("href"));
    if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
      $('sub-navigation-sticky a').removeClass("active");
      currentLink.addClass("active");
    } else {
      currentLink.removeClass("active");
    }
  });
}
/* Quantity */
$(document).ready(function() {
  var n = 1;
  $('.qty-plus').on('click', function() {
    $(".qty-text").val(++n);
  })
  $('.qty-min').on('click', function() {
    if (n >= 1) {
      $(".qty-text").val(--n);
    } else {}
  });
});
// /* menu */
// $(document).ready(function($) {
//   $(".hassubmenu > a").click(function() {
//     if ($(this).next(".submenu").hasClass("open")) {
//       $(this).next(".submenu").find("ul").slideUp(300).removeClass("open");
//       $(this).next(".submenu").find("ul li a").removeClass("activemenu");
//       $(this).next(".submenu").find("ul li a").removeClass("blue");
//       $(this).next(".submenu").slideUp(300);
//       $(this).next(".submenu").removeClass("open");
//       $(this).removeClass("blue");
//       $(this).removeClass("activemenu");
//     } else {
//       $("#canvas-menu-nav > li > ul").each(function() {
//         $(this).slideUp(300).removeClass("open");
//         $(this).parent("li").find("a.activemenu").removeClass("activemenu");
//         $(this).parent("li").find("a.activemenu").removeClass("blue");
//       });
//       $("#canvas-menu-nav > li > ul > li ul").each(function() {
//         $(this).slideUp(300).removeClass("open");
//         $(this).parent("li").find("a.activemenu").removeClass("activemenu");
//         $(this).parent("li").find("a.activemenu").removeClass("blue");
//       });
//       $(this).next(".submenu").slideDown(250);
//       $(this).next(".submenu").addClass("open");
//       $(this).addClass("activemenu");
//       $(this).addClass("blue");
//     }
//   });
// });
// $(document).ready(function($) {
//   $(".haschild > a").click(function() {
//     $(".hassubmenu > a").removeClass("blue");
//     $(".haschild > a").removeClass("blue");
//     if ($(this).next("ul").hasClass("open")) {
//       $(".haschild ul").not($(this).next("ul").slideUp(300));
//       $(this).next("ul").removeClass("open");
//       $(this).removeClass("activemenu");
//       $(this).removeClass("blue");
//     } else {
//       $(".haschild ul").not($(this).next("ul").slideDown(250));
//       $(this).next("ul").addClass("open");
//       $(this).addClass("activemenu");
//       $(this).addClass("blue");
//     }
//   });
// });
/*arrow submenu*/
$(document).ready(function($) {
  $("#SubmenuArrow").click(function() {
    if ($(".nav-submenu").hasClass("open")) {
      $(".nav-submenu").slideUp(300);
      $(".nav-submenu").removeClass("open");
      $(this).removeClass("active");
    } else {
      $(".nav-submenu").slideDown(250);
      $(".nav-submenu").addClass("open");
      $(this).addClass("active");
    }
  });
});
/* Bg Flash */
$(document).ready(function() {
  $(".background-container").css('background', function() {
    return $(this).data('bg')
  });
});
$(document).ready(function() {
  /*Show Password*/
  $(".showpassword").click(function() {
    if ($(this).closest(".form-icon").find(".password").attr("type") == "password") {
      $(this).closest(".form-icon").find(".password").attr("type", "text");
      $(this).find("i").removeClass("icon-eyeo");
      $(this).find("i").addClass("icon-eye");
    } else {
      $(this).closest(".form-icon").find(".password").attr("type", "password");
      $(this).find("i").removeClass("icon-eye");
      $(this).find("i").addClass("icon-eyeo");
    }
  });
  $("#simulasi-drop li").click(function() {
    var datalogo = $(this).find("img").attr("src");
    $("#simulasi-img").attr("src", datalogo);
  });
});
$(function() {
  $(".wishlist-btn").on("click", function() {
    $(this).toggleClass("is-active");
  });
  $("#BrandShowMore").on("click", function() {
    $("#brand-content-arrow").toggleClass("open");
	$(this).toggleClass("active");
  });
});
//Scrolling Header
window.onscroll = changePos;
function changePos() {
    var top_menu_wrap = $('.top-main-navigation');
    // var notif_wrap = $('.notif-wrap');
    if (window.pageYOffset > 10) {
      $('.top-main-navigation').addClass('sticky')
        // top_menu_wrap.slideUp();
    } else {
      $('.top-main-navigation').removeClass('sticky')
        // top_menu_wrap.slideDown();
    }
}